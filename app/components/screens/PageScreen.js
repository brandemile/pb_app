import React from "react";
import { TouchableOpacity } from "react-native";
import { Layout } from "../dumbs";
import {BudgetContainer, PageContainer} from "../smarts";
import { Feather } from "@expo/vector-icons";
import { connect } from "react-redux";

const PageScreen = props => {
  const { navigation } = props;
  const { params } = navigation.state;
  const { key } = params;
  const { name } = params;
    console.log('######################################################');
    console.log('######################################################');
    console.log('######################################################');
  console.log('Key: ' + key);
  console.log('######################################################');
  console.log('######################################################');
  console.log('######################################################');

  return (
    <Layout
      navigation={navigation}
      headerType="headerHasBack"
      title={name}
      goBack={() => navigation.goBack()}
      renderRight={() => (
        <TouchableOpacity
          activeOpacity={0.5}
          onPress={() => navigation.navigate("SearchScreen")}
        >
          <Feather name="search" size={20} color="#fff" />
        </TouchableOpacity>
      )}
      renderContent={() => key == 45391 ? <BudgetContainer navigation={navigation} /> : <PageContainer navigation={navigation} />}
      colorPrimary={props.settings.colorPrimary}
    />
  );
};
const mapStateToProps = state => ({
  settings: state.settings
});
export default connect(mapStateToProps)(PageScreen);
