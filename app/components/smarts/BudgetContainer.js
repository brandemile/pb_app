import React, {PureComponent} from "react";
import {View, Text, StyleSheet, Slider, TextInput, ScrollView, Dimensions} from "react-native";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {
    ListItemTouchable,
    ViewWithLoading,
    isEmpty,
    RequestTimeoutWrapped,
    HtmlViewer
} from "../../wiloke-elements";
import {getPage} from "../../actions";
import * as Consts from "../../constants/styleConstants";
import {screenWidth} from "../../constants/styleConstants";

const {width: SCREEN_WIDTH} = Dimensions.get("window");

class BudgetContainer extends PureComponent {
    state = {
        isLoading: true,
        valueWV: '0',
        valueWP: '0',
        valuePh: '0',
        valueVid: '0',
        valueIS: '0',
        valueWD: '0',
        valueBD: '0',
        valueWS: '0',
        valueSA: '0',
        valueFl: '0',
        valueDH: '0',
        valueDJM: '0',
        valueEnt: '0',
        valueMO: '0',
        valueCH: '0',
        valueO: '0',
        budgetVal: '250000',
        totalExp: '0'
    };
    _getPage = async () => {
        const {navigation} = this.props;
        const {params} = navigation.state;
        const {key} = params;
        await this.props.getPage(key);
        this.setState({isLoading: false});

        const sections = {
            "name": "wedding categories",
            "list": [
                {
                    "name": "Wedding Venue",
                    "default": "0",
                    "key": "",
                },
                {
                    "name": "Wedding Planner",
                    "default": "0",
                    "key": "",
                },
                {
                    "name": "Photography",
                    "default": "0",
                    "key": "",
                },
                {
                    "name": "Videography",
                    "default": "0",
                    "key": "",
                },
                {
                    "name": "Invitations & Stationery",
                    "default": "0",
                    "key": "",
                },
                {
                    "name": "Wedding Dress",
                    "default": "0",
                    "key": "",
                },
                {
                    "name": "Bridesmaid Dresses",
                    "default": "0",
                    "key": "",
                },
                {
                    "name": "Wedding Suit",
                    "default": "0",
                    "key": "",
                },
                {
                    "name": "Shoes & Accessories",
                    "default": "0",
                    "key": "",
                },
                {
                    "name": "Flowers",
                    "default": "0",
                    "key": "",
                },
                {
                    "name": "DJ & Music",
                    "default": "0",
                    "key": "",
                },
                {
                    "name": "Entertainment",
                    "default": "0",
                    "key": "",
                },
                {
                    "name": "Marriage Officer",
                    "default": "0",
                    "key": "",
                },
                {
                    "name": "Car Hire",
                    "default": "0",
                    "key": "",
                }
            ]
        }
    };

    componentDidMount() {
        this._getPage();
    }

    _handlePress = (stackNavListing, item) => () => {
        const {navigation} = this.props;
        navigation.navigate(item.navigation, {
            name: item.name,
            key: item.key
        });
    };

    renderItem = stackNavListing => item => (
        <ListItemTouchable
            key={item.key}
            iconName={item.iconName}
            text={item.name}
            onPress={this._handlePress(stackNavListing, item)}
        />
    );


    render() {
        const {page, navigation} = this.props;
        const {isLoading, budgetVal, totalExp} = this.state;
        const {valueWV, valueWP, valuePh, valueVid, valueIS, valueWD, valueBD, valueWS, valueSA, valueFl, valueDH, valueDJM, valueEnt, valueMO, valueCH, valueO} = this.state;
        const {params} = navigation.state;
        const {key} = params;
        console.log(isLoading);

        const expenses = parseInt(valueWV) + parseInt(valueWP) + parseInt(valuePh) + parseInt(valueVid) + parseInt(valueIS) + parseInt(valueWD) + parseInt(valueBD) + parseInt(valueWS) + parseInt(valueSA) + parseInt(valueFl) + parseInt(valueDH) + parseInt(valueDJM) + parseInt(valueEnt) + parseInt(valueMO) + parseInt(valueCH) + parseInt(valueO);
        const totalBudget = parseInt(budgetVal) - expenses + '.00';

        return (
            <View style={styles.container}>
                <View
                    style={[
                        styles.inner,
                        {
                            width: isLoading ? screenWidth : SCREEN_WIDTH
                        }
                    ]}
                >
                    <ViewWithLoading isLoading={isLoading}>
                        {/* <RequestTimeoutWrapped
          isTimeout={isMenuRequestTimeout}
          onPress={this._getPage}
          fullScreen={true}
          text={translations.networkError}
          buttonText={translations.retry}
        >
          {!isEmpty(stackNavigator) &&
            stackNavigator.map(this.renderItem(stackNavListing))}
        </RequestTimeoutWrapped> */}
                        {!isEmpty(page[key]) && (
                            <ScrollView style={[styles.scrollViewSection]}>
                                <View style={[styles.sectionHead]}>
                                    <Text style={[styles.lightText, styles.smallText, styles.textShadow]}>Budget Breakdown</Text>
                                    <Text style={[styles.lightText, styles.largeText, styles.textShadow]}>R {totalBudget}</Text>
                                </View>
                                <View style={[styles.sectionContent]}>
                                    <View style={[styles.sectionRow]}>
                                        <View style={[styles.budgetInner, styles.boxShadow, {marginRight: 5}]}>
                                            <Text style={[styles.descText]}>Your budget</Text>
                                            <TextInput
                                                style={[styles.textInput]}
                                                onChangeText={budgetVal => this.setState({budgetVal})}
                                                value={budgetVal}
                                                keyboardType='numeric'
                                            />
                                        </View>
                                        <View style={[styles.budgetInner,styles.boxShadow, {marginLeft: 5}]}>
                                            <Text style={[styles.descText]}>Total Expenses</Text>
                                            <Text style={{marginTop: 12}}>R {expenses}.00</Text>
                                        </View>
                                    </View>
                                    <View style={[styles.columnStyle, styles.boxShadow]}>
                                        <Text style={[styles.descText]}>Wedding Venue</Text>
                                        <View style={[styles.sectionRow]}>
                                            <View style={[styles.flex2]}>
                                                <Slider
                                                    value={valueWV}
                                                    step={1}
                                                    minimumValue={0}
                                                    maximumValue={budgetVal}
                                                    onSlidingComplete={valueWV => this.setState({valueWV})}
                                                />
                                            </View>
                                            <View style={[styles.inputField]}>
                                                <TextInput
                                                    style={[styles.textInput]}
                                                    onChangeText={valueWV => this.setState({valueWV})}
                                                    value={valueWV.toString()}
                                                    keyboardType='numeric'
                                                />
                                            </View>

                                        </View>
                                        <Text style={[styles.descText]}>Wedding Planner</Text>
                                        <View style={[styles.sectionRow]}>
                                            <View style={{flex: 2}}>
                                                <Slider
                                                    value={valueWP}
                                                    step={1}
                                                    minimumValue={0}
                                                    maximumValue={budgetVal}
                                                    onSlidingComplete={valueWP => this.setState({valueWP})}
                                                />
                                            </View>
                                            <View style={{flex: 1, paddingLeft: 10}}>
                                                <TextInput
                                                    style={[styles.textInput]}
                                                    onChangeText={valueWP => this.setState({valueWP})}
                                                    value={valueWP.toString()}
                                                    keyboardType='numeric'
                                                />
                                            </View>
                                        </View>
                                        <Text style={[styles.descText]}>Photography</Text>
                                        <View style={[styles.sectionRow]}>
                                            <View style={{flex: 2}}>
                                                <Slider
                                                    value={valuePh}
                                                    step={1}
                                                    minimumValue={0}
                                                    maximumValue={budgetVal}
                                                    onSlidingComplete={valuePh => this.setState({valuePh})}
                                                />
                                            </View>
                                            <View style={{flex: 1, paddingLeft: 10}}>
                                                <TextInput
                                                    style={[styles.textInput]}
                                                    onChangeText={valuePh => this.setState({valuePh})}
                                                    value={valuePh.toString()}
                                                    keyboardType='numeric'
                                                />
                                            </View>
                                        </View>
                                        <Text style={[styles.descText]}>Videography</Text>
                                        <View style={[styles.sectionRow]}>
                                            <View style={{flex: 2}}>
                                                <Slider
                                                    value={valueVid}
                                                    step={1}
                                                    minimumValue={0}
                                                    maximumValue={budgetVal}
                                                    onSlidingComplete={valueVid => this.setState({valueVid})}
                                                />
                                            </View>
                                            <View style={{flex: 1, paddingLeft: 10}}>
                                                <TextInput
                                                    style={[styles.textInput]}
                                                    onChangeText={valueVid => this.setState({valueVid})}
                                                    value={valueVid.toString()}
                                                    keyboardType='numeric'
                                                />
                                            </View>
                                        </View>
                                        <Text style={[styles.descText]}>Invitations & Stationery</Text>
                                        <View style={[styles.sectionRow]}>
                                            <View style={{flex: 2}}>
                                                <Slider
                                                    value={valueIS}
                                                    step={1}
                                                    minimumValue={0}
                                                    maximumValue={budgetVal}
                                                    onSlidingComplete={valueIS => this.setState({valueIS})}
                                                />
                                            </View>
                                            <View style={{flex: 1, paddingLeft: 10}}>
                                                <TextInput
                                                    style={[styles.textInput]}
                                                    onChangeText={valueIS => this.setState({valueIS})}
                                                    value={valueIS.toString()}
                                                    keyboardType='numeric'
                                                />
                                            </View>
                                        </View>
                                        <Text style={[styles.descText]}>Wedding Dress</Text>
                                        <View style={[styles.sectionRow]}>
                                            <View style={{flex: 2}}>
                                                <Slider
                                                    value={valueWD}
                                                    step={1}
                                                    minimumValue={0}
                                                    maximumValue={budgetVal}
                                                    onSlidingComplete={valueWD => this.setState({valueWD})}
                                                />
                                            </View>
                                            <View style={{flex: 1, paddingLeft: 10}}>
                                                <TextInput
                                                    style={[styles.textInput]}
                                                    onChangeText={valueWD => this.setState({valueWD})}
                                                    value={valueWD.toString()}
                                                    keyboardType='numeric'
                                                />
                                            </View>
                                        </View>
                                        <Text style={[styles.descText]}>Bridesmaid Dresses</Text>
                                        <View style={[styles.sectionRow]}>
                                            <View style={{flex: 2}}>
                                                <Slider
                                                    value={valueBD}
                                                    step={1}
                                                    minimumValue={0}
                                                    maximumValue={budgetVal}
                                                    onSlidingComplete={valueBD => this.setState({valueBD})}
                                                />
                                            </View>
                                            <View style={{flex: 1, paddingLeft: 10}}>
                                                <TextInput
                                                    style={[styles.textInput]}
                                                    onChangeText={valueBD => this.setState({valueBD})}
                                                    value={valueBD.toString()}
                                                    keyboardType='numeric'
                                                />
                                            </View>
                                        </View>
                                        <Text style={[styles.descText]}>Wedding Suit</Text>
                                        <View style={[styles.sectionRow]}>
                                            <View style={{flex: 2}}>
                                                <Slider
                                                    value={valueWS}
                                                    step={1}
                                                    minimumValue={0}
                                                    maximumValue={budgetVal}
                                                    onSlidingComplete={valueWS => this.setState({valueWS})}
                                                />
                                            </View>
                                            <View style={{flex: 1, paddingLeft: 10}}>
                                                <TextInput
                                                    style={[styles.textInput]}
                                                    onChangeText={valueWS => this.setState({valueWS})}
                                                    value={valueWS.toString()}
                                                    keyboardType='numeric'
                                                />
                                            </View>
                                        </View>
                                        <Text style={[styles.descText]}>Shoes & Accessories</Text>
                                        <View style={[styles.sectionRow]}>
                                            <View style={{flex: 2}}>
                                                <Slider
                                                    value={valueSA}
                                                    step={1}
                                                    minimumValue={0}
                                                    maximumValue={budgetVal}
                                                    onSlidingComplete={valueSA => this.setState({valueSA})}
                                                />
                                            </View>
                                            <View style={{flex: 1, paddingLeft: 10}}>
                                                <TextInput
                                                    style={[styles.textInput]}
                                                    onChangeText={valueSA => this.setState({valueSA})}
                                                    value={valueSA.toString()}
                                                    keyboardType='numeric'
                                                />
                                            </View>
                                        </View>
                                        <Text style={[styles.descText]}>Flowers</Text>
                                        <View style={[styles.sectionRow]}>
                                            <View style={{flex: 2}}>
                                                <Slider
                                                    value={valueFl}
                                                    step={1}
                                                    minimumValue={0}
                                                    maximumValue={budgetVal}
                                                    onSlidingComplete={valueFl => this.setState({valueFl})}
                                                />
                                            </View>
                                            <View style={{flex: 1, paddingLeft: 10}}>
                                                <TextInput
                                                    style={[styles.textInput]}
                                                    onChangeText={valueFl => this.setState({valueFl})}
                                                    value={valueFl.toString()}
                                                    keyboardType='numeric'
                                                />
                                            </View>
                                        </View>
                                        <Text style={[styles.descText]}>Decor & Hiring</Text>
                                        <View style={[styles.sectionRow]}>
                                            <View style={{flex: 2}}>
                                                <Slider
                                                    value={valueDH}
                                                    step={1}
                                                    minimumValue={0}
                                                    maximumValue={budgetVal}
                                                    onSlidingComplete={valueDH => this.setState({valueDH})}
                                                />
                                            </View>
                                            <View style={{flex: 1, paddingLeft: 10}}>
                                                <TextInput
                                                    style={[styles.textInput]}
                                                    onChangeText={valueDH => this.setState({valueDH})}
                                                    value={valueDH.toString()}
                                                    keyboardType='numeric'
                                                />
                                            </View>
                                        </View>
                                        <Text style={[styles.descText]}>DJ & Music</Text>
                                        <View style={[styles.sectionRow]}>
                                            <View style={{flex: 2}}>
                                                <Slider
                                                    value={valueDJM}
                                                    step={1}
                                                    minimumValue={0}
                                                    maximumValue={budgetVal}
                                                    onSlidingComplete={valueDJM => this.setState({valueDJM})}
                                                />
                                            </View>
                                            <View style={{flex: 1, paddingLeft: 10}}>
                                                <TextInput
                                                    style={[styles.textInput]}
                                                    onChangeText={valueDJM => this.setState({valueDJM})}
                                                    value={valueDJM.toString()}
                                                    keyboardType='numeric'
                                                />
                                            </View>
                                        </View>
                                        <Text style={[styles.descText]}>Entertainment</Text>
                                        <View style={[styles.sectionRow]}>
                                            <View style={{flex: 2}}>
                                                <Slider
                                                    value={valueEnt}
                                                    step={1}
                                                    minimumValue={0}
                                                    maximumValue={budgetVal}
                                                    onSlidingComplete={valueEnt => this.setState({valueEnt})}
                                                />
                                            </View>
                                            <View style={{flex: 1, paddingLeft: 10}}>
                                                <TextInput
                                                    style={[styles.textInput]}
                                                    onChangeText={valueEnt => this.setState({valueEnt})}
                                                    value={valueEnt.toString()}
                                                    keyboardType='numeric'
                                                />
                                            </View>
                                        </View>
                                        <Text style={[styles.descText]}>Marriage Officer</Text>
                                        <View style={[styles.sectionRow]}>
                                            <View style={{flex: 2}}>
                                                <Slider
                                                    value={valueMO}
                                                    step={1}
                                                    minimumValue={0}
                                                    maximumValue={budgetVal}
                                                    onSlidingComplete={valueMO => this.setState({valueMO})}
                                                />
                                            </View>
                                            <View style={{flex: 1, paddingLeft: 10}}>
                                                <TextInput
                                                    style={[styles.textInput]}
                                                    onChangeText={valueMO => this.setState({valueMO})}
                                                    value={valueMO.toString()}
                                                    keyboardType='numeric'
                                                />
                                            </View>
                                        </View>
                                        <Text style={[styles.descText]}>Car Hire</Text>
                                        <View style={[styles.sectionRow]}>
                                            <View style={{flex: 2}}>
                                                <Slider
                                                    value={valueCH}
                                                    step={1}
                                                    minimumValue={0}
                                                    maximumValue={budgetVal}
                                                    onSlidingComplete={valueCH => this.setState({valueCH})}
                                                />
                                            </View>
                                            <View style={{flex: 1, paddingLeft: 10}}>
                                                <TextInput
                                                    style={[styles.textInput]}
                                                    onChangeText={valueCH => this.setState({valueCH})}
                                                    value={valueCH.toString()}
                                                    keyboardType='numeric'
                                                />
                                            </View>
                                        </View>
                                        <Text style={[styles.descText]}>Other</Text>
                                        <View style={[styles.sectionRow]}>
                                            <View style={{flex: 2}}>
                                                <Slider
                                                    value={valueO}
                                                    step={1}
                                                    minimumValue={0}
                                                    maximumValue={budgetVal}
                                                    onSlidingComplete={valueO => this.setState({valueO})}
                                                />
                                            </View>
                                            <View style={{flex: 1, paddingLeft: 10}}>
                                                <TextInput
                                                    style={[styles.textInput]}
                                                    onChangeText={valueO => this.setState({valueO})}
                                                    value={valueO.toString()}
                                                    keyboardType='numeric'
                                                />
                                            </View>
                                        </View>
                                    </View>
                                </View>


                            </ScrollView>
                        )}
                    </ViewWithLoading>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: SCREEN_WIDTH,
        alignItems: "center"
    },
    inner: {
        flex: 1,
        height: "100%"
    },
    scrollViewSection: {
        flexDirection: 'column',
        backgroundColor: '#efefef',
        width: '100%',
        flex: 1,
    },
    sectionHead: {
        backgroundColor: '#eea1ad',
        textAlign: 'center',
        alignItems: 'center',
        height: 180,
        paddingTop: 50
    },
    lightText: {
        color: '#ffffff'
    },
    smallText: {
        fontSize: 14
    },
    largeText: {
        fontSize: 36,
        fontWeight: 'bold'
    },
    textShadow: {
        textShadowColor: 'rgba(102, 102, 102, 0.4)',
        textShadowOffset: {width: 0, height: 1},
        textShadowRadius: 3
    },
    descText: {
        fontSize: 12,
        fontWeight: 'bold',
        marginBottom: 3
    },
    sectionContent: {
        marginTop: -60,
        padding: 20,
    },
    budgetInner: {
        flex: 1,
        padding: 10,
        borderRadius: 5,
        backgroundColor: '#ffffff',
    },
    boxShadow: {
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 7
    },
    columnStyle: {
        padding: 10,
        borderRadius: 5,
        backgroundColor: '#ffffff',
        flexDirection: 'column',
    },
    sectionRow: {
        flexDirection: 'row',
        marginBottom: 10,
        borderBottomWidth: 1,
        borderColor: '#efefef',
        paddingBottom: 10
    },
    flex2: {
        flex: 2,
    },
    inputField: {
        flex: 1,
        paddingLeft: 10
    },
    textInput: {
        padding: 5,
        height: 40,
        borderColor: '#efefef',
        borderWidth: 1,
        borderRadius: 3
    }
});

const mapStateToProps = state => ({
    page: state.page,
    translations: state.translations
});

const mapDispatchToProps = {
    getPage
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BudgetContainer);
